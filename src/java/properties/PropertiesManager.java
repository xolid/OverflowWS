/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package properties;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.Set;

/**
 *
 * @author czara
 */
public class PropertiesManager {
    
    private Properties properties = new Properties();
    URL url;
    
    public PropertiesManager(){
        try {
            url = getClass().getResource("/properties/Overflow.properties");
            properties.load(url.openStream());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void setProperty(String key,String value){
        properties.setProperty(key, value);
    }
    
    public String getProperty(String key){
        return properties.getProperty(key);
    }
    
    public void saveProperties(){
        FileWriter writer;
        try {
            writer = new FileWriter(new File(url.getPath()));
            properties.store(writer, "settings");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public Set<Object> getProperties(){
        return properties.keySet();
    }
    
}
