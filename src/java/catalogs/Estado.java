/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package catalogs;

/**
 *
 * @author czara
 */
public class Estado{
    
    private final String[] VALUES = {"PENDIENTE","RECIBIDA","RECHAZADA","EN PROGRESO","COMPLETADA","DECLINADA","APROBADA","CANCELADA"};
    
    public String getNombre(int index){
        return VALUES[index];
    }
    
    public int getValor(String nombre){
        for(int index=0;index<VALUES.length;index++){
            if(VALUES[index].equals(nombre))
                return index;
        }
        return -1;
    }
    
    public String[] getEstados(){
        return VALUES;
    }
    
}