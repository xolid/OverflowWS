package beans;
// Generated Aug 14, 2018 5:06:26 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Asignacion generated by hbm2java
 */
public class Asignacion  implements java.io.Serializable {


     private Integer idasignacion;
     private Grupo grupo;
     private Usuario usuario;
     private Date finicio;
     private Date ftermino;

    public Asignacion() {
    }

	
    public Asignacion(Grupo grupo, Usuario usuario, Date finicio) {
        this.grupo = grupo;
        this.usuario = usuario;
        this.finicio = finicio;
    }
    public Asignacion(Grupo grupo, Usuario usuario, Date finicio, Date ftermino) {
       this.grupo = grupo;
       this.usuario = usuario;
       this.finicio = finicio;
       this.ftermino = ftermino;
    }
   
    public Integer getIdasignacion() {
        return this.idasignacion;
    }
    
    public void setIdasignacion(Integer idasignacion) {
        this.idasignacion = idasignacion;
    }
    public Grupo getGrupo() {
        return this.grupo;
    }
    
    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }
    public Usuario getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    public Date getFinicio() {
        return this.finicio;
    }
    
    public void setFinicio(Date finicio) {
        this.finicio = finicio;
    }
    public Date getFtermino() {
        return this.ftermino;
    }
    
    public void setFtermino(Date ftermino) {
        this.ftermino = ftermino;
    }




}


