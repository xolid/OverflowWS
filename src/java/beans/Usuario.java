package beans;
// Generated Aug 14, 2018 5:06:26 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Usuario generated by hbm2java
 */
public class Usuario  implements java.io.Serializable {


     private Integer idusuario;
     private Rol rol;
     private String nombre;
     private String puesto;
     private String mail;
     private String pass;
     private Date fingreso;
     private Date fegreso;
     private byte[] foto;
     private Set comentarios = new HashSet(0);
     private Set adjuntos = new HashSet(0);
     private Set converzacions = new HashSet(0);
     private Set flujos = new HashSet(0);
     private Set asignacions = new HashSet(0);
     private Set mensajes = new HashSet(0);
     private Set tareas = new HashSet(0);
     private Set invitados = new HashSet(0);

    public Usuario() {
    }

	
    public Usuario(Rol rol, String nombre, String puesto, String mail, String pass, Date fingreso) {
        this.rol = rol;
        this.nombre = nombre;
        this.puesto = puesto;
        this.mail = mail;
        this.pass = pass;
        this.fingreso = fingreso;
    }
    public Usuario(Rol rol, String nombre, String puesto, String mail, String pass, Date fingreso, Date fegreso, byte[] foto, Set comentarios, Set adjuntos, Set converzacions, Set flujos, Set asignacions, Set mensajes, Set tareas, Set invitados) {
       this.rol = rol;
       this.nombre = nombre;
       this.puesto = puesto;
       this.mail = mail;
       this.pass = pass;
       this.fingreso = fingreso;
       this.fegreso = fegreso;
       this.foto = foto;
       this.comentarios = comentarios;
       this.adjuntos = adjuntos;
       this.converzacions = converzacions;
       this.flujos = flujos;
       this.asignacions = asignacions;
       this.mensajes = mensajes;
       this.tareas = tareas;
       this.invitados = invitados;
    }
   
    public Integer getIdusuario() {
        return this.idusuario;
    }
    
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }
    public Rol getRol() {
        return this.rol;
    }
    
    public void setRol(Rol rol) {
        this.rol = rol;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getPuesto() {
        return this.puesto;
    }
    
    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }
    public String getMail() {
        return this.mail;
    }
    
    public void setMail(String mail) {
        this.mail = mail;
    }
    public String getPass() {
        return this.pass;
    }
    
    public void setPass(String pass) {
        this.pass = pass;
    }
    public Date getFingreso() {
        return this.fingreso;
    }
    
    public void setFingreso(Date fingreso) {
        this.fingreso = fingreso;
    }
    public Date getFegreso() {
        return this.fegreso;
    }
    
    public void setFegreso(Date fegreso) {
        this.fegreso = fegreso;
    }
    public byte[] getFoto() {
        return this.foto;
    }
    
    public void setFoto(byte[] foto) {
        this.foto = foto;
    }
    public Set getComentarios() {
        return this.comentarios;
    }
    
    public void setComentarios(Set comentarios) {
        this.comentarios = comentarios;
    }
    public Set getAdjuntos() {
        return this.adjuntos;
    }
    
    public void setAdjuntos(Set adjuntos) {
        this.adjuntos = adjuntos;
    }
    public Set getConverzacions() {
        return this.converzacions;
    }
    
    public void setConverzacions(Set converzacions) {
        this.converzacions = converzacions;
    }
    public Set getFlujos() {
        return this.flujos;
    }
    
    public void setFlujos(Set flujos) {
        this.flujos = flujos;
    }
    public Set getAsignacions() {
        return this.asignacions;
    }
    
    public void setAsignacions(Set asignacions) {
        this.asignacions = asignacions;
    }
    public Set getMensajes() {
        return this.mensajes;
    }
    
    public void setMensajes(Set mensajes) {
        this.mensajes = mensajes;
    }
    public Set getTareas() {
        return this.tareas;
    }
    
    public void setTareas(Set tareas) {
        this.tareas = tareas;
    }
    public Set getInvitados() {
        return this.invitados;
    }
    
    public void setInvitados(Set invitados) {
        this.invitados = invitados;
    }




}


