package beans;
// Generated Aug 14, 2018 5:06:26 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Grupo generated by hbm2java
 */
public class Grupo  implements java.io.Serializable {


     private Integer idgrupo;
     private String nombre;
     private Set actividads = new HashSet(0);
     private Set asignacions = new HashSet(0);

    public Grupo() {
    }

	
    public Grupo(String nombre) {
        this.nombre = nombre;
    }
    public Grupo(String nombre, Set actividads, Set asignacions) {
       this.nombre = nombre;
       this.actividads = actividads;
       this.asignacions = asignacions;
    }
   
    public Integer getIdgrupo() {
        return this.idgrupo;
    }
    
    public void setIdgrupo(Integer idgrupo) {
        this.idgrupo = idgrupo;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public Set getActividads() {
        return this.actividads;
    }
    
    public void setActividads(Set actividads) {
        this.actividads = actividads;
    }
    public Set getAsignacions() {
        return this.asignacions;
    }
    
    public void setAsignacions(Set asignacions) {
        this.asignacions = asignacions;
    }




}


