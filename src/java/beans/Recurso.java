package beans;
// Generated Aug 14, 2018 5:06:26 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Recurso generated by hbm2java
 */
public class Recurso  implements java.io.Serializable {


     private Integer idrecurso;
     private Rol rol;
     private String nombre;
     private String descripcion;
     private int tipo;
     private Date fecha;
     private byte[] contenido;

    public Recurso() {
    }

	
    public Recurso(String nombre, String descripcion, int tipo, Date fecha, byte[] contenido) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.fecha = fecha;
        this.contenido = contenido;
    }
    public Recurso(Rol rol, String nombre, String descripcion, int tipo, Date fecha, byte[] contenido) {
       this.rol = rol;
       this.nombre = nombre;
       this.descripcion = descripcion;
       this.tipo = tipo;
       this.fecha = fecha;
       this.contenido = contenido;
    }
   
    public Integer getIdrecurso() {
        return this.idrecurso;
    }
    
    public void setIdrecurso(Integer idrecurso) {
        this.idrecurso = idrecurso;
    }
    public Rol getRol() {
        return this.rol;
    }
    
    public void setRol(Rol rol) {
        this.rol = rol;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public int getTipo() {
        return this.tipo;
    }
    
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    public Date getFecha() {
        return this.fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public byte[] getContenido() {
        return this.contenido;
    }
    
    public void setContenido(byte[] contenido) {
        this.contenido = contenido;
    }




}


