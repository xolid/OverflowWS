/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package navigation;

import beans.*;
import facades.*;
import java.util.List;

/**
 *
 * @author czarate
 */
public class Flow{
    
    public String createFlow(Usuario usuario, Flujo flujo){
        String result = "OPERACION NO REALIZADA";
        FlujoFacade facade = new FlujoFacade();
        int idflujo = facade.saveFlujo(flujo);
        flujo = facade.getFlujoByID(idflujo);
        tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Se crea el flujo",flujo);
        String mensaje =  notifications.Template.notificationFlujo("Se abrio una nueva solicitud en el sistema.",flujo);
        new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Nueva solicitud en Overflow",mensaje);
        result = "SE CREA LA SOLICITUD";
        facade.close();
        return result;
    }
    
    public String updateFlow(Usuario usuario, Flujo flujo){
        String result = "OPERACION NO REALIZADA";
        FlujoFacade facade = new FlujoFacade();
        facade.updateFlujo(flujo);
        tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Se actualiza el flujo",flujo);
        String mensaje =  notifications.Template.notificationFlujo("Se actualizo un flujo en el sistema.",flujo);
        new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Actualizacion de solicitud en Overflow",mensaje);
        result = "SE ACTUALIZA FLUJO";
        facade.close();
        return result;
    }
    
    public String aproveFlow(int idusuario, int idflujo){
        String result = "OPERACION NO REALIZADA";
        FlujoFacade facade = new FlujoFacade();
        UsuarioFacade ufacade = new UsuarioFacade();
        Flujo flujo = facade.getFlujoByID(idflujo);
        if(flujo!=null){
            if(flujo.getEstado()==4){
                Usuario usuario = ufacade.getUsuarioByID(idusuario);
                flujo.setFterminado(new java.util.Date());
                flujo.setEstado(6);
                facade.updateFlujo(flujo);
                tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(flujo.getEstado())+"]",flujo);
                String mensaje =  notifications.Template.notificationFlujoCE("Cambio de estado en flujo.",flujo);
                new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Actualizacion de flujo en Overflow",mensaje);
                result = "SE APRUEBA SOLICITUD";
            }
        }
        ufacade.close();
        facade.close();
        return result;
    }
    
    public String cancelFlow(int idusuario, int idflujo){
        String result = "OPERACION NO REALIZADA";
        FlujoFacade facade = new FlujoFacade();
        UsuarioFacade ufacade = new UsuarioFacade();
        Flujo flujo = facade.getFlujoByID(idflujo);
        Usuario usuario = ufacade.getUsuarioByID(idusuario);
        cancelAllTaskByFlow(usuario,flujo);
        flujo.setFterminado(new java.util.Date());
        flujo.setEstado(7);
        facade.updateFlujo(flujo);
        tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(flujo.getEstado())+"]",flujo);
        String mensaje =  notifications.Template.notificationFlujoCE("Cambio de estado en flujo.",flujo);
        new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Actualizacion de flujo en Overflow",mensaje);
        result = "SE APLICA CANCELACION";
        ufacade.close();
        facade.close();
        return result;
    }
    
    private void cancelAllTaskByFlow(Usuario usuario, Flujo flujo){
        TareaFacade tfacade = new TareaFacade();
        java.util.List<beans.Tarea> tareas = tfacade.getTareaByFlujo(flujo.getIdflujo());
        for(beans.Tarea tarea:tareas){
            if(tarea.getEstado()!=4){
                tarea.setEstado(7);
            }
            tfacade.updateTarea(tarea);
            tools.LogRegister.write(usuario.getNombre(),tarea.getActividad().getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(tarea.getEstado())+"]",tarea.getFlujo());
            if(tarea.getUsuario()!=null){
                String mensaje =  notifications.Template.notificationTarea("Se ha cancelado la tarea que tenia asignada en el sistema Overflow.",tarea);
                new notifications.EmailSender().sendEmail(tarea.getUsuario().getMail(),"Cancelacion de Tarea en Overflow",mensaje);
            }
        }
        tfacade.close();
    }
    
    public String sendFlow(int idusuario, int idflujo){
        String result = "OPERACION NO REALIZADA";
        FlujoFacade facade = new FlujoFacade();
        UsuarioFacade ufacade = new UsuarioFacade();
        Flujo flujo = facade.getFlujoByID(idflujo);
        if(flujo!=null){
            if(flujo.getEstado()==0){
                Usuario usuario = ufacade.getUsuarioByID(idusuario);
                createTasksByFlow(usuario,flujo);
                Tarea first = openFirstTaskByFlow(usuario,flujo);
                flujo.setEstado(first.getEstado());
                flujo.setFsolicitado(first.getFrecibido());
                facade.updateFlujo(flujo);
                tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(flujo.getEstado())+"]",flujo);
                String mensaje =  notifications.Template.notificationFlujoCE("Cambio de estado en flujo.",flujo);
                new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Actualizacion de flujo en Overflow",mensaje);
                result = "SE ENVIA SOLICITUD";
            }
        }
        ufacade.close();
        facade.close();
        return result;
    }
    
    private void createTasksByFlow(Usuario usuario, Flujo flujo){
        ActividadFacade facade = new ActividadFacade();
        TareaFacade tfacade = new TareaFacade();
        for(Actividad actividad:facade.getActividadByProceso(flujo.getProceso().getIdproceso())){
            Tarea tarea = new Tarea();
            tarea.setIdtarea(0);
            tarea.setActividad(actividad);
            tarea.setFlujo(flujo);
            tarea.setUsuario(null);
            tarea.setFrecibido(null);
            tarea.setFasignado(null);
            tarea.setFproceso(null);
            tarea.setFliberado(null);
            tarea.setEstado(0);
            tfacade.saveTarea(tarea);
            tools.LogRegister.write(usuario.getNombre(),tarea.getActividad().getNombre(),"Se crea la tarea en estado ["+new catalogs.Estado().getNombre(tarea.getEstado())+"]",flujo);
        }
        tfacade.close();
        facade.close();
    }
    
    private Tarea openFirstTaskByFlow(Usuario usuario, Flujo flujo){
        TareaFacade facade = new TareaFacade();
        AsignacionFacade asfacade = new AsignacionFacade();
        Tarea tarea = facade.getFirstTareaByFlujo(flujo.getIdflujo());
        java.util.List<String> correos = asfacade.getAsignacionListMailByGrupo(tarea.getActividad().getGrupo().getIdgrupo());
        if(tarea!=null){
            tarea.setFrecibido(new java.util.Date());
            tarea.setEstado(1);
            java.util.List<beans.Asignacion> asignaciones = asfacade.getAsignacionByGrupo(tarea.getActividad().getGrupo().getIdgrupo());
            if(asignaciones!=null){
                if(asignaciones.size()==1){
                    for(beans.Asignacion asignacion:asignaciones){ 
                        tools.Hora hora = new tools.Hora(new java.util.Date());
                        java.util.Date date = hora.validHour(new java.util.Date());
                        tarea.setFasignado(new java.util.Date());
                        tarea.setFproceso(hora.addHoursInValidTime(date,tarea.getActividad().getDuracion()));
                        tarea.setUsuario(asignacion.getUsuario());
                        tarea.setEstado(3);
                    }
                }
            }
            tools.LogRegister.write(usuario.getNombre(),tarea.getActividad().getNombre(),"Se envia la tarea y cambia de estado a ["+new catalogs.Estado().getNombre(tarea.getEstado())+"]",flujo);
            facade.updateTarea(tarea);
        }
        if(tarea.getUsuario()!=null){
            String mensaje =  notifications.Template.notificationTarea("Se le ha asignado una tarea en el sistema Overflow.",tarea);
            new notifications.EmailSender().sendEmail(tarea.getUsuario().getMail(),"Asignacion de Tarea en Overflow",mensaje);
        }else{
            String mensaje =  notifications.Template.notificationTarea("Una tarea ha arribado a su grupo de asignacion en el sistema Overflow.",tarea);
            new notifications.EmailSender().sendMultipleEmail(correos,"Tarea sin asignacion en Overflow",mensaje);
        }
        asfacade.close();
        facade.close();
        return tarea;
    }
    
    public String completeFlow(int idusuario, int idflujo){
        String result = "OPERACION NO REALIZADA";
        FlujoFacade facade = new FlujoFacade();
        UsuarioFacade ufacade = new UsuarioFacade();
        TareaFacade tfacade = new TareaFacade();
        Flujo flujo = facade.getFlujoByID(idflujo);
        if(flujo!=null){
            if(flujo.getEstado()==2){
                Usuario usuario = ufacade.getUsuarioByID(idusuario);
                tools.Hora hora = new tools.Hora(new java.util.Date());
                java.util.Date date = hora.validHour(new java.util.Date());
                Tarea tarea = tfacade.getFirstTareaByFlujo(flujo.getIdflujo());
                if(tarea!=null){
                    tarea.setFrecibido(new java.util.Date());
                    tarea.setFasignado(new java.util.Date());
                    tarea.setFproceso(hora.addHoursInValidTime(date,tarea.getActividad().getDuracion()));
                    tarea.setEstado(3);
                    tfacade.updateTarea(tarea);
                    tools.LogRegister.write(usuario.getNombre(),tarea.getActividad().getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(tarea.getEstado())+"]",flujo);
                    String mensaje1 =  notifications.Template.notificationTarea("Se le ha asignado una tarea en el sistema Overflow.",tarea);
                    new notifications.EmailSender().sendEmail(tarea.getUsuario().getMail(),"Asignacion de Tarea en Overflow",mensaje1);
                    flujo.setFsolicitado(new java.util.Date());
                    flujo.setEstado(3);
                    facade.updateFlujo(flujo);
                    tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(flujo.getEstado())+"]",flujo);
                    String mensaje2 =  notifications.Template.notificationFlujoCE("Cambio de estado en flujo.",flujo);
                    new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Actualizacion de flujo en Overflow",mensaje2);
                    result = "SE COMPLETO TAREA";
                }
            }
        }
        tfacade.close();
        ufacade.close();
        facade.close();
        return result;
    }
    
    public String declineFlow(int idusuario, int idflujo){
        String result = "OPERACION NO REALIZADA";
        FlujoFacade facade = new FlujoFacade();
        UsuarioFacade ufacade = new UsuarioFacade();
        Flujo flujo = facade.getFlujoByID(idflujo);
        if(flujo!=null){
            if(flujo.getEstado()==4){
                Usuario usuario = ufacade.getUsuarioByID(idusuario);
                resetTasksByFlow(usuario, flujo);
                Tarea first = openFirstTaskByFlow(usuario, flujo);
                flujo.setEstado(first.getEstado());
                flujo.setFsolicitado(first.getFrecibido());
                flujo.setFentregado(null);
                facade.updateFlujo(flujo);
                tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(flujo.getEstado())+"]",flujo);
                String mensaje =  notifications.Template.notificationFlujoCE("Cambio de estado en flujo.",flujo);
                new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Actualizacion de flujo en Overflow",mensaje);
                result = "SE DECLINO LA SOLICITUD";
            }
        }
        ufacade.close();
        facade.close();
        return result;
    }
    
    private void resetTasksByFlow(Usuario usuario, Flujo flujo){
        TareaFacade facade = new TareaFacade();
        for(Tarea tarea:facade.getTareaByFlujo(flujo.getIdflujo())){
            tarea.setUsuario(null);
            tarea.setFrecibido(null);
            tarea.setFasignado(null);
            tarea.setFproceso(null);
            tarea.setFliberado(null);
            tarea.setEstado(0);
            facade.updateTarea(tarea);
            tools.LogRegister.write(usuario.getNombre(),tarea.getActividad().getNombre(),"Se reinicia la tarea en estado ["+new catalogs.Estado().getNombre(tarea.getEstado())+"]",flujo);
        }
        facade.close();
    }
    
    public String reasignTask(int idexecuser, int iddestuser, int idtarea){
        String result = "OPERACION NO REALIZADA";
        TareaFacade facade = new TareaFacade();
        FlujoFacade ffacade = new FlujoFacade();
        UsuarioFacade ufacade = new UsuarioFacade();
        Usuario execuser = ufacade.getUsuarioByID(idexecuser);
        Usuario destuser = ufacade.getUsuarioByID(iddestuser);
        Tarea tarea = facade.getTareaByID(idtarea);
        if(tarea!=null){
            if(tarea.getEstado()==2||tarea.getEstado()==3){
                if(destuser==null){
                    removeAsign(execuser,tarea);
                    AsignacionFacade asfacade = new AsignacionFacade();
                    java.util.List<String> correos = asfacade.getAsignacionListMailByGrupo(tarea.getActividad().getGrupo().getIdgrupo());
                    String mensaje =  notifications.Template.notificationTarea("Una tarea ha arribado a su grupo de asignacion en el sistema Overflow.",tarea);
                    new notifications.EmailSender().sendMultipleEmail(correos,"Tarea sin asignacion en Overflow",mensaje);
                    asfacade.close();
                    result = "SE LIBERA TAREA";
                }else{
                    if(tarea!=null){
                        if(tarea.getUsuario()==null){
                            tools.Hora hora = new tools.Hora(new java.util.Date());
                            java.util.Date date = hora.validHour(new java.util.Date());
                            tarea.setUsuario(destuser);
                            tarea.setFasignado(new java.util.Date());
                            tarea.setFproceso(hora.addHoursInValidTime(date,tarea.getActividad().getDuracion()));
                            tarea.setEstado(3);
                            beans.Tarea anterior = facade.getPreviousTareaByFlujo(tarea.getFlujo().getIdflujo(),tarea.getIdtarea());
                            if(anterior==null){
                                beans.Flujo flujo = ffacade.getFlujoByID(tarea.getFlujo().getIdflujo());
                                flujo.setEstado(3);
                                ffacade.updateFlujo(flujo);
                                tools.LogRegister.write(execuser.getNombre(),flujo.getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(flujo.getEstado())+"]",flujo);
                                String mensaje =  notifications.Template.notificationFlujoCE("Cambio de estado en flujo.",flujo);
                                new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Actualizacion de flujo en Overflow",mensaje);
                            }
                            tools.LogRegister.write(execuser.getNombre(),tarea.getActividad().getNombre()," Se reasigna y cambia a estado ["+new catalogs.Estado().getNombre(tarea.getEstado())+"]",tarea.getFlujo());
                            result = "SE APLICA ASIGNACION";
                        }else{
                            tarea.setUsuario(destuser);
                            result = "SE APLICA REASIGNACION";
                        }
                        facade.updateTarea(tarea);
                        String mensaje =  notifications.Template.notificationTarea("Se le ha asignado una tarea en el sistema Overflow.",tarea);
                        new notifications.EmailSender().sendEmail(tarea.getUsuario().getMail(),"Asignacion de Tarea en Overflow",mensaje);
                    }
                }
            }
        }
        ufacade.close();
        ffacade.close();
        facade.close();     
        return result;
    }
    
    private void removeAsign(Usuario usuario, Tarea tarea){
        TareaFacade facade = new TareaFacade();
        FlujoFacade ffacade = new FlujoFacade();
        tarea.setUsuario(null);
        tarea.setFasignado(null);
        tarea.setFproceso(null);
        tarea.setEstado(1);
        facade.updateTarea(tarea);
        tools.LogRegister.write(usuario.getNombre(),tarea.getActividad().getNombre(),"Retira asignacion actual",tarea.getFlujo());
        
        beans.Tarea anterior = facade.getPreviousTareaByFlujo(tarea.getFlujo().getIdflujo(),tarea.getIdtarea());
        if(anterior==null){
            beans.Flujo flujo = ffacade.getFlujoByID(tarea.getFlujo().getIdflujo());
            flujo.setEstado(1);
            ffacade.updateFlujo(flujo);
            tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(flujo.getEstado())+"]",flujo);
            String mensaje =  notifications.Template.notificationFlujoCE("Cambio de estado en flujo.",flujo);
            new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Actualizacion de flujo en Overflow",mensaje);
        }
        ffacade.close();
        facade.close();
    }
    
    public String completeTask(int idusuario, int idtarea){
        String result = "OPERACION NO REALIZADA";
        TareaFacade facade = new TareaFacade();
        UsuarioFacade ufacade = new UsuarioFacade();
        FlujoFacade ffacade = new FlujoFacade();
        Tarea tarea = facade.getTareaByID(idtarea);
        if(tarea!=null){
            if(tarea.getEstado()==3){
                Usuario usuario = ufacade.getUsuarioByID(idusuario);
                tarea.setFliberado(new java.util.Date());
                tarea.setEstado(4);
                facade.updateTarea(tarea);
                tools.LogRegister.write(usuario.getNombre(),tarea.getActividad().getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(tarea.getEstado())+"]",tarea.getFlujo());
                beans.Tarea siguiente = facade.getNextTareaByFlujo(tarea.getFlujo().getIdflujo(),tarea.getIdtarea());
                if(siguiente!=null){
                    openTask(usuario,siguiente);
                }else{
                    beans.Flujo flujo = ffacade.getFlujoByID(tarea.getFlujo().getIdflujo());
                    flujo.setFentregado(new java.util.Date());
                    flujo.setEstado(4);
                    ffacade.updateFlujo(flujo);
                    tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(flujo.getEstado())+"]",flujo);
                    String mensaje =  notifications.Template.notificationFlujoCE("Cambio de estado en flujo.",flujo);
                    new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Actualizacion de flujo en Overflow",mensaje);
                }
                result = "SE COMPLETA TAREA";
            }
        }
        ffacade.close();
        ufacade.close();
        facade.close();
        return result;
    }
    
    private void openTask(Usuario usuario, Tarea siguiente){
        TareaFacade facade = new TareaFacade();
        AsignacionFacade asfacade = new AsignacionFacade();
        List<String> correos = asfacade.getAsignacionListMailByGrupo(siguiente.getActividad().getGrupo().getIdgrupo());
        tools.Hora hora = new tools.Hora(new java.util.Date());
        java.util.Date date = hora.validHour(new java.util.Date());
        siguiente.setFrecibido(new java.util.Date());
        siguiente.setEstado(1);      
        if(siguiente.getUsuario()==null){
            java.util.List<beans.Asignacion> asignaciones = asfacade.getAsignacionByGrupo(siguiente.getActividad().getGrupo().getIdgrupo());
            if(asignaciones!=null){
                if(asignaciones.size()==1){
                    for(beans.Asignacion asignacion:asignaciones){
                        siguiente.setFasignado(new java.util.Date());
                        siguiente.setFproceso(hora.addHoursInValidTime(date,siguiente.getActividad().getDuracion()));
                        siguiente.setUsuario(asignacion.getUsuario());
                        siguiente.setEstado(3);
                    }
                }
            }
        }else{
            siguiente.setEstado(3);   
            siguiente.setFasignado(new java.util.Date());
            siguiente.setFproceso(hora.addHoursInValidTime(date,siguiente.getActividad().getDuracion()));
        }
        facade.updateTarea(siguiente);
        tools.LogRegister.write(usuario.getNombre(),siguiente.getActividad().getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(siguiente.getEstado())+"]",siguiente.getFlujo());
        if(siguiente.getFasignado()!=null){
            String mensaje =  notifications.Template.notificationTarea("Se le ha asignado una tarea en el sistema Overflow.",siguiente);
            new notifications.EmailSender().sendEmail(siguiente.getUsuario().getMail(),"Asignacion de Tarea en Overflow",mensaje);
        }else{
            String mensaje =  notifications.Template.notificationTarea("Una tarea ha arribado a su grupo de asignacion en el sistema Overflow.",siguiente);
            new notifications.EmailSender().sendMultipleEmail(correos,"Tarea sin asignacion en Overflow",mensaje);
        }
        asfacade.close();
        facade.close();
    }
    
    public String progressTask(int idusuario, int idtarea){
        String result = "OPERACION NO REALIZADA";
        TareaFacade facade = new TareaFacade();
        FlujoFacade ffacade = new FlujoFacade();
        UsuarioFacade ufacade = new UsuarioFacade();
        Tarea tarea = facade.getTareaByID(idtarea);
        if(tarea!=null){
            if(tarea.getEstado()==2){
                Usuario usuario = ufacade.getUsuarioByID(idusuario);
                tools.Hora hora = new tools.Hora(new java.util.Date());
                java.util.Date date = hora.validHour(new java.util.Date());
                tarea.setFasignado(new java.util.Date());
                tarea.setFproceso(hora.addHoursInValidTime(date,tarea.getActividad().getDuracion()));
                tarea.setUsuario(usuario);
                tarea.setEstado(3);
                facade.updateTarea(tarea);
                tools.LogRegister.write(usuario.getNombre(),tarea.getActividad().getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(tarea.getEstado())+"]",tarea.getFlujo());
                String mensaje1 =  notifications.Template.notificationTarea("Usted a ocupado una tarea en el sistema Overflow.",tarea);
                new notifications.EmailSender().sendEmail(tarea.getUsuario().getMail(),"Asignacion de Tarea en Overflow",mensaje1);
                beans.Tarea anterior = facade.getPreviousTareaByFlujo(tarea.getFlujo().getIdflujo(),tarea.getIdtarea());
                if(anterior==null){
                    beans.Flujo flujo = ffacade.getFlujoByID(tarea.getFlujo().getIdflujo());
                    flujo.setEstado(3);
                    ffacade.updateFlujo(flujo);
                    tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(flujo.getEstado())+"]",flujo);
                    String mensaje2 =  notifications.Template.notificationFlujoCE("Cambio de estado en flujo.",flujo);
                    new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Actualizacion de flujo en Overflow",mensaje2);
                }
                result = "SE OCUPA TAREA";
            }
        }
        ufacade.close();
        ffacade.close();
        facade.close();
        return result;
    }
    
    public String refuseTask(int idusuario, int idtarea){
        String result = "OPERACION NO REALIZADA";
        TareaFacade facade = new TareaFacade();
        FlujoFacade ffacade = new FlujoFacade();
        UsuarioFacade ufacade = new UsuarioFacade();        
        Tarea tarea = facade.getTareaByID(idtarea);
        if(tarea!=null){
            if(tarea.getEstado()==3){
                Usuario usuario = ufacade.getUsuarioByID(idusuario);
                tarea.setFrecibido(null);
                tarea.setFasignado(null);
                tarea.setFproceso(null);
                tarea.setFliberado(null);
                tarea.setEstado(0);
                facade.updateTarea(tarea);
                tools.LogRegister.write(usuario.getNombre(),tarea.getActividad().getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(tarea.getEstado())+"]",tarea.getFlujo());
                beans.Tarea anterior = facade.getPreviousTareaByFlujo(tarea.getFlujo().getIdflujo(),tarea.getIdtarea());
                if(anterior!=null){
                    anterior.setEstado(2);
                    anterior.setFliberado(null);
                    facade.updateTarea(anterior);
                    tools.LogRegister.write(usuario.getNombre(),anterior.getActividad().getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(anterior.getEstado())+"]",anterior.getFlujo());
                    String mensaje =  notifications.Template.notificationTarea("Una tarea completada por usted ha sido rechazada en el sistema Overflow.",anterior);
                    new notifications.EmailSender().sendEmail(anterior.getUsuario().getMail(),"Rechazo de Tarea en Overflow",mensaje);
                }else{
                    beans.Flujo flujo = ffacade.getFlujoByID(tarea.getFlujo().getIdflujo());
                    flujo.setFsolicitado(null);
                    flujo.setEstado(2);
                    ffacade.updateFlujo(flujo);
                    tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Cambio a estado ["+new catalogs.Estado().getNombre(flujo.getEstado())+"]",flujo);
                    String mensaje =  notifications.Template.notificationFlujoCE("Cambio de estado en flujo.",flujo);
                    new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Actualizacion de flujo en Overflow",mensaje);
                }
                result = "SE APLICA RECHAZO";
            }
        }
        ufacade.close();
        ffacade.close();
        facade.close();
        return result;
    }
    
}
