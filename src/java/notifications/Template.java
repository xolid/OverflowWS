/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notifications;

/**
 *
 * @author czarate
 */
public class Template {
    
    public static String notificationFlujo(String mensaje, beans.Flujo flujo){
        String body = "<h1>"+mensaje+"</h1>";
        body += "<strong>Folio de Flujo:</strong> "+flujo.getIdflujo()+"<br/>";
        body += "<strong>Fecha Creacion:</strong> "+new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFcreado())+"<br/>";
        body += "<strong>Fecha Requerido:</strong> "+new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFrequerido())+"<br/><br/>";
        body += "<strong>Requerimiento:</strong> "+flujo.getNombre()+"<br/>";
        body += "<strong>Descripcion:</strong><br/>"+flujo.getDescripcion()+"<br/><br/>";
        body += "<a href=\""+new properties.PropertiesManager().getProperty("URL")+"\">IR A OVERLOAD<a/>";
        return body;
    }
    
    public static String notificationFlujoCE(String mensaje, beans.Flujo flujo){
        String body = "<h1>"+mensaje+"</h1>";
        body += "<strong>Folio de Flujo:</strong> "+flujo.getIdflujo()+"<br/>";
        body += "<strong>Fecha:</strong> "+new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+"<br/><br/>";
        body += "<strong>Nuevo Estado:</strong> "+new catalogs.Estado().getNombre(flujo.getEstado())+"<br/><br/>";
        body += "<a href=\""+new properties.PropertiesManager().getProperty("URL")+"\">IR A OVERLOAD<a/>";
        return body;
    }
    
    public static String notificationTarea(String mensaje, beans.Tarea tarea){
        String body = "<h1>"+mensaje+"</h1>";
        body += "<strong>Folio de Tarea:</strong> "+tarea.getIdtarea()+"<br/>";
        body += "<strong>Fecha de Recibido:</strong> "+new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFrecibido())+"<br/><br/>";
        body += "<strong>Proceso:</strong> "+tarea.getFlujo().getProceso().getNombre()+"<br/>";
        body += "<strong>Actividad:</strong> "+tarea.getActividad().getNombre()+"<br/><br/>";
        body += "<strong>Tarea:</strong> "+tarea.getFlujo().getNombre()+"<br/>";
        body += "<strong>Descripcion:</strong> "+tarea.getFlujo().getNombre()+"<br/><br/>";
        body += "<a href=\""+new properties.PropertiesManager().getProperty("URL")+"\">IR A OVERLOAD<a/>";
        return body;
    }
    
}
