/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservices;

import beans.Adjunto;
import beans.Comentario;
import beans.Flujo;
import beans.Proceso;
import beans.Registro;
import beans.Tarea;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import navigation.Flow;

/**
 *
 * @author czarate
 */
@WebService(serviceName = "WSRedactar")
public class WSRedactar {

    public WSRedactar(){
        
    }

    /**
     * Web service operation
     * @param mail
     * @param pass
     * @return 
     */
    @WebMethod(operationName = "getAllByUsuario")
    public java.util.List<java.lang.String[]> getAllByUsuario(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getFlujoListData(usuario.getIdusuario());
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getFlujoListData(int idusuario){
        facades.FlujoFacade ffacade = new facades.FlujoFacade();
        List<String[]> list = new ArrayList();
        for(Flujo flujo:ffacade.getFlujoByUsuario(idusuario)){
            String[] arreglo = new String[11];
            arreglo[0] = String.valueOf(flujo.getIdflujo());
            arreglo[1] = flujo.getProceso().getNombre();
            arreglo[2] = flujo.getUsuario().getNombre();
            arreglo[3] = flujo.getNombre();
            arreglo[4] = flujo.getDescripcion();
            arreglo[5] = flujo.getFcreado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFcreado());
            arreglo[6] = flujo.getFrequerido()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFrequerido());
            arreglo[7] = flujo.getFsolicitado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFsolicitado());
            arreglo[8] = flujo.getFentregado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFentregado());
            arreglo[9] = flujo.getFterminado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFterminado());
            arreglo[10] = new catalogs.Estado().getNombre(flujo.getEstado());
            list.add(arreglo);
        }
        ffacade.close();
        return list;
    }
    
/**
     * Web service operation
     * @param mail
     * @param pass
     * @return 
     */
    @WebMethod(operationName = "getAllByUsuarioTASKCOUNT")
    public java.util.List<java.lang.String[]> getAllByUsuarioTASKCOUNT(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getFlujoTASKCOUNTListData(usuario.getIdusuario());
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getFlujoTASKCOUNTListData(int idusuario){
        facades.FlujoFacade ffacade = new facades.FlujoFacade();
        List<String[]> list = new ArrayList();
        for(Object[] flujo:ffacade.getFlujoByUsuarioTASK_COUNT(idusuario)){
            String[] arreglo = new String[13];
            arreglo[0] = String.valueOf(flujo[0]);
            arreglo[1] = String.valueOf(flujo[1]);
            arreglo[2] = String.valueOf(flujo[2]);
            arreglo[3] = String.valueOf(flujo[3]);
            arreglo[4] = String.valueOf(flujo[4]);
            arreglo[5] = flujo[5]==null?"":String.valueOf(flujo[5]);
            arreglo[6] = flujo[6]==null?"":String.valueOf(flujo[6]);
            arreglo[7] = flujo[7]==null?"":String.valueOf(flujo[7]);
            arreglo[8] = flujo[8]==null?"":String.valueOf(flujo[8]);
            arreglo[9] = flujo[9]==null?"":String.valueOf(flujo[9]);
            arreglo[10] = new catalogs.Estado().getNombre(Integer.parseInt(String.valueOf(flujo[10])));
            arreglo[11] = String.valueOf(flujo[11]);
            arreglo[12] = String.valueOf(flujo[12]);
            list.add(arreglo);
        }
        ffacade.close();
        return list;
    }
    
    /**
     * Web service operation
     * @param mail
     * @param pass
     * @return 
     */
    @WebMethod(operationName = "getAllByUsuarioCLOSED")
    public java.util.List<java.lang.String[]> getAllByUsuarioCLOSED(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getFlujoListDataCLOSED(usuario.getIdusuario());
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getFlujoListDataCLOSED(int idusuario){
        facades.FlujoFacade ffacade = new facades.FlujoFacade();
        List<String[]> list = new ArrayList();
        java.util.Calendar hace90dias = java.util.Calendar.getInstance();
        hace90dias.add(java.util.Calendar.DAY_OF_MONTH,-90);
        for(Flujo flujo:ffacade.getFlujoByUsuarioCLOSED(idusuario,hace90dias.getTime())){
            String[] arreglo = new String[11];
            arreglo[0] = String.valueOf(flujo.getIdflujo());
            arreglo[1] = flujo.getProceso().getNombre();
            arreglo[2] = flujo.getUsuario().getNombre();
            arreglo[3] = flujo.getNombre();
            arreglo[4] = flujo.getDescripcion();
            arreglo[5] = flujo.getFcreado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFcreado());
            arreglo[6] = flujo.getFrequerido()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFrequerido());
            arreglo[7] = flujo.getFsolicitado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFsolicitado());
            arreglo[8] = flujo.getFentregado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFentregado());
            arreglo[9] = flujo.getFterminado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFterminado());
            arreglo[10] = new catalogs.Estado().getNombre(flujo.getEstado());
            list.add(arreglo);
        }
        ffacade.close();
        return list;
    }
    
    /**
     * Web service operation
     * @param mail
     * @param pass
     * @return 
     */
    @WebMethod(operationName = "getAllByUsuarioCLOSEDTASKCOUNT")
    public java.util.List<java.lang.String[]> getAllByUsuarioCLOSEDTASKCOUNT(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getFlujoListDataCLOSEDTASKCOUNT(usuario.getIdusuario());
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getFlujoListDataCLOSEDTASKCOUNT(int idusuario){
        facades.FlujoFacade ffacade = new facades.FlujoFacade();
        List<String[]> list = new ArrayList();
        java.util.Calendar hace90dias = java.util.Calendar.getInstance();
        hace90dias.add(java.util.Calendar.DAY_OF_MONTH,-90);
        for(Object[] flujo:ffacade.getFlujoByUsuarioCLOSEDTASK_COUNT(idusuario,hace90dias.getTime())){
            String[] arreglo = new String[13];
            arreglo[0] = String.valueOf(flujo[0]);
            arreglo[1] = String.valueOf(flujo[1]);
            arreglo[2] = String.valueOf(flujo[2]);
            arreglo[3] = String.valueOf(flujo[3]);
            arreglo[4] = String.valueOf(flujo[4]);
            arreglo[5] = flujo[5]==null?"":String.valueOf(flujo[5]);
            arreglo[6] = flujo[6]==null?"":String.valueOf(flujo[6]);
            arreglo[7] = flujo[7]==null?"":String.valueOf(flujo[7]);
            arreglo[8] = flujo[8]==null?"":String.valueOf(flujo[8]);
            arreglo[9] = flujo[9]==null?"":String.valueOf(flujo[9]);
            arreglo[10] = new catalogs.Estado().getNombre(Integer.parseInt(String.valueOf(flujo[10])));
            arreglo[11] = String.valueOf(flujo[11]);
            arreglo[12] = String.valueOf(flujo[12]);
            list.add(arreglo);
        }
        ffacade.close();
        return list;
    }

    /**
     * Web service operation
     * @param mail
     * @param pass
     * @param idflujo
     * @return 
     */
    @WebMethod(operationName = "getFlujoByUsuario")
    public java.lang.String[] getFlujoByUsuario(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass, @WebParam(name = "idflujo") int idflujo) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getFlujoData(idflujo);
            }else{
                ufacade.close();
                return new String[11];
            }
        }
    }
    
    private String[] getFlujoData(int idflujo){
        facades.FlujoFacade ffacade = new facades.FlujoFacade();
        Flujo flujo = ffacade.getFlujoByID(idflujo);
        String[] arreglo = new String[11];
        arreglo[0] = String.valueOf(flujo.getIdflujo());
        arreglo[1] = flujo.getProceso().getNombre();
        arreglo[2] = flujo.getUsuario().getNombre();
        arreglo[3] = flujo.getNombre();
        arreglo[4] = flujo.getDescripcion();
        arreglo[5] = flujo.getFcreado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFcreado());
        arreglo[6] = flujo.getFrequerido()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFrequerido());
        arreglo[7] = flujo.getFsolicitado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFsolicitado());
        arreglo[8] = flujo.getFentregado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFentregado());
        arreglo[9] = flujo.getFterminado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFterminado());
        arreglo[10] = new catalogs.Estado().getNombre(flujo.getEstado());
        ffacade.close();
        return arreglo;
    }
    
    /**
     * Web service operation
     * @param mail
     * @param pass
     * @param idflujo
     * @return 
     */
    @WebMethod(operationName = "getAllTareasByFlujo")
    public List<String[]> getALLTareasByFlujo(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass, @WebParam(name = "idflujo") int idflujo) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getTareaListData(idflujo);
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getTareaListData(int idflujo){
        facades.TareaFacade tfacade = new facades.TareaFacade();
        List<String[]> list = new ArrayList();
        for(Tarea tarea:tfacade.getTareaByFlujo(idflujo)){
            String[] arreglo = new String[9];
            arreglo[0] = String.valueOf(tarea.getIdtarea());
            arreglo[1] = tarea.getActividad().getNombre();
            arreglo[2] = tarea.getFlujo().getNombre();
            arreglo[3] = tarea.getUsuario()==null?"SIN ASIGNACION":tarea.getUsuario().getNombre();
            arreglo[4] = tarea.getFrecibido()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFrecibido());
            arreglo[5] = tarea.getFasignado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFasignado());
            arreglo[6] = tarea.getFproceso()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFproceso());
            arreglo[7] = tarea.getFliberado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFliberado());
            arreglo[8] = new catalogs.Estado().getNombre(tarea.getEstado());
            list.add(arreglo);
        }
        tfacade.close();
        return list;
    }
    
    /**
     * Web service operation
     * @param mail
     * @param pass
     * @param idflujo
     * @return 
     */
    @WebMethod(operationName = "getAllAdjuntosByFlujo")
    public List<String[]> getAllAdjuntosByFlujo(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass, @WebParam(name = "idflujo") int idflujo) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getAdjuntoListData(idflujo);
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getAdjuntoListData(int idflujo){
        facades.AdjuntoFacade afacade = new facades.AdjuntoFacade();
        List<String[]> list = new ArrayList();
        for(Adjunto adjunto:afacade.getAdjuntoByFlujo(idflujo)){
            String[] arreglo = new String[5];
            arreglo[0] = String.valueOf(adjunto.getIdadjunto());
            arreglo[1] = adjunto.getFlujo().getNombre();
            arreglo[2] = adjunto.getUsuario().getNombre();
            arreglo[3] = adjunto.getNombre();
            arreglo[4] = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(adjunto.getFecha());
            list.add(arreglo);
        }
        afacade.close();
        return list;
    }
    
    /**
     * Web service operation
     * @param mail
     * @param pass
     * @param idflujo
     * @return 
     */
    @WebMethod(operationName = "getAllComentariosByFlujo")
    public List<String[]> getALLComentariosByFlujo(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass, @WebParam(name = "idflujo") int idflujo) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getComentarioListData(idflujo);
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getComentarioListData(int idflujo){
        facades.ComentarioFacade cfacade = new facades.ComentarioFacade();
        List<String[]> list = new ArrayList();
        for(Comentario comentario:cfacade.getComentarioByFlujo(idflujo)){
            String[] arreglo = new String[5];
            arreglo[0] = String.valueOf(comentario.getIdcomentario());
            arreglo[1] = comentario.getFlujo().getNombre();
            arreglo[2] = comentario.getUsuario().getNombre();
            arreglo[3] = comentario.getTexto();
            arreglo[4] = comentario.getFecha()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(comentario.getFecha());
            list.add(arreglo);
        }
        cfacade.close();
        return list;
    }
    
/**
     * Web service operation
     * @param mail
     * @param pass
     * @param idflujo
     * @return 
     */
    @WebMethod(operationName = "getAllRegistrosByFlujo")
    public List<String[]> getALLRegistrosByFlujo(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass, @WebParam(name = "idflujo") int idflujo) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getRegistroListData(idflujo);
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getRegistroListData(int idflujo){
        facades.RegistroFacade rfacade = new facades.RegistroFacade();
        List<String[]> list = new ArrayList();
        for(Registro registro:rfacade.getRegistroByFlujo(idflujo)){
            String[] arreglo = new String[6];
            arreglo[0] = String.valueOf(registro.getIdregistro());
            arreglo[1] = registro.getFlujo().getNombre();
            arreglo[2] = registro.getDispara();
            arreglo[3] = registro.getObjetivo();
            arreglo[4] = registro.getAccion();
            arreglo[5] = registro.getFecha()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(registro.getFecha());
            list.add(arreglo);
        }
        rfacade.close();
        return list;
    }

    /**
     * Web service operation
     * @param mail
     * @param pass
     * @return 
     */
    @WebMethod(operationName = "getDataByEstado")
    public List<String[]> getDataByEstado(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getEstadoListData(usuario.getIdusuario());
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getEstadoListData(int idusuario){
        facades.FlujoFacade ffacade = new facades.FlujoFacade();
        List<String[]> list = new ArrayList();
        for(Object[] objeto:ffacade.getFlujoByEstadoCountANDGroupBy(idusuario)){
            String[] arreglo = new String[2];
            arreglo[0] = new catalogs.Estado().getNombre(Integer.parseInt(String.valueOf(objeto[0])));
            arreglo[1] = String.valueOf(objeto[1]);
            list.add(arreglo);
        }
        ffacade.close();
        return list;
    }
    

    /**
     * Web service operation
     * @param mail
     * @param pass
     * @return 
     */
    @WebMethod(operationName = "getDataByProceso")
    public List<String[]> getDataByProceso(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getProcesoListData(usuario.getIdusuario());
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getProcesoListData(int idusuario){
        facades.FlujoFacade ffacade = new facades.FlujoFacade();
        List<String[]> list = new ArrayList();
        for(Object[] objeto:ffacade.getFlujoByProcesoCountANDGroupBy(idusuario)){
            String[] arreglo = new String[2];
            arreglo[0] = String.valueOf(objeto[0]);
            arreglo[1] = String.valueOf(objeto[1]);
            list.add(arreglo);
        }
        ffacade.close();
        return list;
    }    
    
    /**
     * Web service operation
     * @param mail
     * @param pass
     * @return 
     */
    @WebMethod(operationName = "getDataByTiempo")
    public List<String[]> getDataByTiempo(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getTiempoListData(usuario.getIdusuario());
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getTiempoListData(int idusuario){
        facades.FlujoFacade ffacade = new facades.FlujoFacade();
        List<String[]> list = new ArrayList();
        for(Flujo flujo:ffacade.getFlujoByUsuario(idusuario)){
            String[] arreglo = new String[4];
            arreglo[0] = String.valueOf(flujo.getIdflujo());
            arreglo[1] = flujo.getNombre();
            if(flujo.getFrequerido()!=null){
                long diffRequerido = flujo.getFrequerido().getTime() - new java.util.Date().getTime();
                arreglo[2] = String.valueOf(((diffRequerido / 1000) / 60) / 60);
            }else{
                arreglo[2] = "0";
            }
            if(flujo.getFsolicitado()!=null){
                long diffSolicitado = new java.util.Date().getTime() - flujo.getFsolicitado().getTime();
                arreglo[3] = String.valueOf(((diffSolicitado / 1000) / 60) / 60);
            }else{
                arreglo[3] = "0";
            }
            list.add(arreglo);
        }
        ffacade.close();
        return list;
    }  
    
    /**
     * Web service operation
     * @param mail
     * @param pass
     * @return 
     */
    @WebMethod(operationName = "loginData")
    public java.lang.String[] loginData(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            String[] noexist = {"-1","undefined","undefined"};
            ufacade.close();
            return noexist;
        }else{
            if(usuario.getRol().getRedacta()==1){
                String[] data = {String.valueOf(usuario.getIdusuario()),usuario.getNombre(),usuario.getMail()};
                ufacade.close();
                return data;
            }else{
                String[] nogrant = {"0","nogrant","nogrant"};
                ufacade.close();
                return nogrant;
            }
        }
    }

    /**
     * Web service operation
     * @param mail
     * @param pass
     * @param nombre
     * @param descripcion
     * @param frequerido
     * @param proceso
     * @return 
     */
    @WebMethod(operationName = "createFlujo")
    public int createFlujo(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass, @WebParam(name = "nombre") String nombre, @WebParam(name = "descripcion") String descripcion, @WebParam(name = "frequerido") String frequerido, @WebParam(name = "proceso") String proceso) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return -1;
        }else{
            if(usuario.getRol().getRedacta()==1){
                facades.ProcesoFacade pfacade = new facades.ProcesoFacade();
                facades.FlujoFacade ffacade = new facades.FlujoFacade();
                Flujo flujo = new Flujo();
                flujo.setIdflujo(0);
                flujo.setNombre(nombre);
                flujo.setDescripcion(descripcion);
                flujo.setFcreado(new java.util.Date());
                java.util.Date requerido = new java.util.Date();
                try{
                    requerido = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(frequerido);
                }catch(ParseException e) { }
                flujo.setFrequerido(requerido);
                flujo.setEstado(0);
                flujo.setProceso(pfacade.getProcesoByNAME(proceso));
                flujo.setUsuario(usuario);
                int id = ffacade.saveFlujo(flujo);
                flujo = ffacade.getFlujoByID(id);

                tools.LogRegister.write(usuario.getNombre(),flujo.getNombre(),"Se crea el flujo via app movil",flujo);
                String mensaje =  notifications.Template.notificationFlujo("Se abrio una nueva solicitud en el sistema.",flujo);
                new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Nueva solicitud en Overflow",mensaje);
                
                ufacade.close();
                pfacade.close();
                ffacade.close();
                return id;
            }else{
                ufacade.close();
                return 0;
            }
        }
    }
    
    /**
     * Web service operation
     * @param mail
     * @param pass
     * @param texto
     * @param idflujo
     * @return 
     */
    @WebMethod(operationName = "createComentario")
    public int createComentario(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass, @WebParam(name = "texto") String texto, @WebParam(name = "idflujo") int idflujo) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return -1;
        }else{
            if(usuario.getRol().getRedacta()==1){
                facades.FlujoFacade ffacade = new facades.FlujoFacade();
                facades.ComentarioFacade cfacade = new facades.ComentarioFacade();
                Comentario comentario = new Comentario();
                comentario.setIdcomentario(0);
                comentario.setFlujo(ffacade.getFlujoByID(idflujo));
                comentario.setUsuario(usuario);
                comentario.setTexto(texto);
                comentario.setFecha(new java.util.Date());
                int id = cfacade.saveComentario(comentario);
                cfacade.close();
                ffacade.close();
                ufacade.close();
                tools.LogRegister.write(usuario.getNombre(),comentario.getTexto(),"Sube comentario via app movil",comentario.getFlujo());
                return id;
            }else{
                ufacade.close();
                return 0;
            }
        }
    }

    /**
     * Web service operation
     * @param mail
     * @param pass
     * @return 
     */
    @WebMethod(operationName = "getAllProceso")
    public List<String[]> getAllProceso(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getProcesoListData();
            }else{
                ufacade.close();
                return new ArrayList();
            }
        }
    }
    
    private List<String[]> getProcesoListData(){
        facades.ProcesoFacade pfacade = new facades.ProcesoFacade();
        List<String[]> list = new ArrayList();
        for(Proceso proceso:pfacade.getProcesoAll()){
            String[] arreglo = new String[2];
            arreglo[0] = String.valueOf(proceso.getIdproceso());
            arreglo[1] = proceso.getNombre();
            list.add(arreglo);
        }
        pfacade.close();
        return list;
    }  

    /**
     * Web service operation
     * @param mail
     * @param pass
     * @param nombre
     * @param contenido
     * @param idflujo
     * @return 
     */
    @WebMethod(operationName = "createAdjunto")
    public int createAdjunto(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass, @WebParam(name = "nombre") String nombre, @WebParam(name = "contenido") String contenido, @WebParam(name = "idflujo") int idflujo) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return -1;
        }else{
            if(usuario.getRol().getRedacta()==1){
                facades.FlujoFacade ffacade = new facades.FlujoFacade();
                facades.AdjuntoFacade afacade = new facades.AdjuntoFacade();
                Adjunto adjunto = new Adjunto();
                adjunto.setIdadjunto(0);
                adjunto.setFlujo(ffacade.getFlujoByID(idflujo));
                adjunto.setUsuario(usuario);
                adjunto.setNombre(nombre);
                adjunto.setFecha(new java.util.Date());
                byte[] decodificado = new byte[0];
                try{
                    decodificado = Base64.getDecoder().decode(contenido.getBytes("UTF-8"));
                }catch(UnsupportedEncodingException e){ }
                adjunto.setContenido(decodificado);
                int id = afacade.saveAdjunto(adjunto);
                afacade.close();
                ffacade.close();
                ufacade.close();
                tools.LogRegister.write(usuario.getNombre(),adjunto.getNombre(),"Adjunta archivo via app movil",adjunto.getFlujo());
                return id;
            }else{
                ufacade.close();
                return 0;
            }
        }
    }
    
    /**
     * Web service operation
     * @param mail
     * @param pass
     * @param idadjunto
     * @return 
     */
    @WebMethod(operationName = "getAdjuntoById")
    public String[] getAdjuntoById(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass, @WebParam(name = "idadjunto") int idadjunto) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return null;
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                return getAdjuntoData(idadjunto);
            }else{
                ufacade.close();
                return new String[0];
            }
        }
    }
    
    private String[] getAdjuntoData(int idadjunto){
        facades.AdjuntoFacade afacade = new facades.AdjuntoFacade();
        Adjunto adjunto = afacade.getAdjuntoByID(idadjunto);
        String[] arreglo = new String[6];
        arreglo[0] = String.valueOf(adjunto.getIdadjunto());
        arreglo[1] = adjunto.getFlujo().getNombre();
        arreglo[2] = adjunto.getUsuario().getNombre();
        arreglo[3] = adjunto.getNombre();
        arreglo[4] = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(adjunto.getFecha());
        arreglo[5] = new String(Base64.getEncoder().encode(adjunto.getContenido()));
        afacade.close();
        return arreglo;
    }

    /**
     * Web service operation
     * @param mail
     * @param pass
     * @param operation
     * @param idflujo
     * @return 
     */
    @WebMethod(operationName = "changeStatus")
    public String changeStatus(@WebParam(name = "mail") String mail, @WebParam(name = "pass") String pass, @WebParam(name = "operation") String operation, @WebParam(name = "idflujo") int idflujo) {
        facades.UsuarioFacade ufacade = new facades.UsuarioFacade();
        beans.Usuario usuario = ufacade.getUsuarioByMailPass(mail, pass);
        if(usuario==null){
            ufacade.close();
            return "USUARIO NO EXISTE";
        }else{
            if(usuario.getRol().getRedacta()==1){
                ufacade.close();
                String resultado = "";
                switch(operation){
                    case "cancelFlow":
                        resultado = new Flow().cancelFlow(usuario.getIdusuario(),idflujo);
                        break;
                    case "aproveFlow":
                        resultado = new Flow().aproveFlow(usuario.getIdusuario(),idflujo);
                        break;
                    case "completeFlow":
                        resultado = new Flow().completeFlow(usuario.getIdusuario(),idflujo);
                        break;
                    case "declineFlow":
                        resultado = new Flow().declineFlow(usuario.getIdusuario(),idflujo);
                        break;
                    case "sendFlow":
                        resultado = new Flow().sendFlow(usuario.getIdusuario(),idflujo);
                        break;
                }
                
                return resultado;
            }else{
                ufacade.close();
                return "USUARIO NO REDACTOR";
            }
        }
    }
    
}
