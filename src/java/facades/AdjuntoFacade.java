/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Adjunto;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class AdjuntoFacade {

    private Session session;

    public AdjuntoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public Integer saveAdjunto(Adjunto adjunto) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(adjunto);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public Adjunto getAdjuntoByID(int idadjunto) {
        Adjunto adjunto = (Adjunto) session.get(Adjunto.class, idadjunto);
        return adjunto;
    }

    public List<Adjunto> getAdjuntoAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Adjunto as adjunto order by nombre");
        List<Adjunto> adjuntos = (List<Adjunto>) q.list();
        session.getTransaction().commit();
        return adjuntos;
    }
    
    public List<Adjunto> getAdjuntoByFlujo(int idflujo) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Adjunto as adjunto where flujo_idflujo = :idflujo");
        q.setInteger("idflujo", idflujo);
        List<Adjunto> adjuntos = (List<Adjunto>) q.list();
        session.getTransaction().commit();
        return adjuntos;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
