/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Rol;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class RolFacade {

    private Session session;

    public RolFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Rol getRolByID(int idrol) {
        Rol rol = (Rol) session.get(Rol.class, idrol);
        return rol;
    }

    public List<Rol> getRolAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Rol as rol order by nombre");
        List<Rol> rols = (List<Rol>) q.list();
        session.getTransaction().commit();
        return rols;
    }
    
    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
