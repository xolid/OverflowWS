/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Recurso;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class RecursoFacade {

    private Session session;

    public RecursoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Recurso getRecursoByID(int idrecurso) {
        Recurso recurso = (Recurso) session.get(Recurso.class, idrecurso);
        return recurso;
    }

    public Integer saveRecurso(Recurso recurso) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(recurso);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateRecurso(Recurso recurso) {
        try {
            session.getTransaction().begin();
            Recurso o = getRecursoByID(recurso.getIdrecurso());
            o.setNombre(recurso.getNombre());
            o.setDescripcion(recurso.getDescripcion());
            o.setTipo(recurso.getTipo());
            o.setFecha(recurso.getFecha());
            o.setContenido(recurso.getContenido());
            o.setRol(recurso.getRol());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteRecurso(Recurso recurso) {
        try {
            session.getTransaction().begin();
            Recurso o = getRecursoByID(recurso.getIdrecurso());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Recurso> getRecursoAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Recurso as recurso");
        List<Recurso> recursos = (List<Recurso>) q.list();
        session.getTransaction().commit();
        return recursos;
    }
    
    public List<Recurso> getRecursoByTipo(int tipo) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Recurso as recurso where tipo = :tipo");
        q.setInteger("tipo", tipo);
        List<Recurso> recursos = (List<Recurso>) q.list();
        session.getTransaction().commit();
        return recursos;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Recurso as recurso");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
