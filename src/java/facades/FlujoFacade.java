/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Flujo;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class FlujoFacade {

    private Session session;

    public FlujoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Flujo getFlujoByID(int idflujo) {
        Flujo flujo = (Flujo) session.get(Flujo.class, idflujo);
        return flujo;
    }
    
    public Integer saveFlujo(Flujo flujo) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(flujo);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }
    
    public void updateFlujo(Flujo flujo) {
        try {
            session.getTransaction().begin();
            Flujo o = getFlujoByID(flujo.getIdflujo());
            o.setNombre(flujo.getNombre());
            o.setDescripcion(flujo.getDescripcion());
            o.setFcreado(flujo.getFcreado());
            o.setFrequerido(flujo.getFrequerido());
            o.setFsolicitado(flujo.getFsolicitado());
            o.setFentregado(flujo.getFentregado());
            o.setFterminado(flujo.getFterminado());
            o.setEstado(flujo.getEstado());
            o.setUsuario(flujo.getUsuario());
            o.setProceso(flujo.getProceso());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Flujo> getFlujoAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Flujo as flujo order by nombre");
        List<Flujo> flujos = (List<Flujo>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public List<Flujo> getFlujoByUsuario(int idusuario) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Flujo as flujo where usuario_idusuario = :idusuario and estado < 6 order by fcreado desc");
        q.setInteger("idusuario",idusuario);
        List<Flujo> flujos = (List<Flujo>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public List<Object[]> getFlujoByUsuarioTASK_COUNT(int idusuario) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select " +
                    "idflujo, " +
                    "(select nombre from proceso where idproceso = f.proceso_idproceso) as proceso, " +
                    "(select nombre from usuario where idusuario = f.usuario_idusuario) as usuario, " +
                    "nombre, " +
                    "descripcion, " +
                    "fcreado, " +
                    "frequerido, " +
                    "fsolicitado, " +
                    "fentregado, " +
                    "fterminado, " +
                    "estado, " +
                    "(select count(*) from tarea where flujo_idflujo = f.idflujo) as tareas, " +
                    "(select count(*) from tarea where flujo_idflujo = f.idflujo and estado = 4 and fliberado is not null) as cerrados " +
                    "from " +
                    "flujo as f " +
                    "where " +
                    "f.usuario_idusuario = :idusuario and " +
                    "f.estado < 6 " +
                    "order by " +
                    "f.fcreado desc");
        q.setInteger("idusuario",idusuario);
        List<Object[]> flujos = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public List<Flujo> getFlujoByUsuarioCLOSED(int idusuario, java.util.Date fecha) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Flujo as flujo where usuario_idusuario = :idusuario and estado > 5 and fterminado > :fecha order by fcreado desc");
        q.setInteger("idusuario",idusuario);
        q.setDate("fecha", fecha);
        List<Flujo> flujos = (List<Flujo>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public List<Object[]> getFlujoByUsuarioCLOSEDTASK_COUNT(int idusuario, java.util.Date fecha) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select " +
                    "idflujo, " +
                    "(select nombre from proceso where idproceso = f.proceso_idproceso) as proceso, " +
                    "(select nombre from usuario where idusuario = f.usuario_idusuario) as usuario, " +
                    "nombre, " +
                    "descripcion, " +
                    "fcreado, " +
                    "frequerido, " +
                    "fsolicitado, " +
                    "fentregado, " +
                    "fterminado, " +
                    "estado, " +
                    "(select count(*) from tarea where flujo_idflujo = f.idflujo) as tareas, " +
                    "(select count(*) from tarea where flujo_idflujo = f.idflujo and estado = 4 and fliberado is not null) as cerrados " +
                    "from " +
                    "flujo as f " +
                    "where " +
                    "f.usuario_idusuario = :idusuario and " +
                    "f.estado > 5 and " +
                    "f.fterminado > :fecha " +
                    "order by " +
                    "f.fcreado desc");
        q.setInteger("idusuario",idusuario);
        q.setDate("fecha", fecha);
        List<Object[]> flujos = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public List<Object[]> getFlujoByProcesoCountANDGroupBy(int idusuario) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT proceso.nombre,count(*) FROM flujo inner join proceso on flujo.proceso_idproceso = proceso.idproceso where usuario_idusuario = :idusuario and estado < 6 group by proceso.idproceso");
        q.setInteger("idusuario", idusuario);
        java.util.List<Object[]> flujos = (java.util.List<Object[]>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public List<Object[]> getFlujoByEstadoCountANDGroupBy(int idusuario) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT estado,count(*) FROM flujo where usuario_idusuario = :idusuario and estado < 6 group by estado");
        q.setInteger("idusuario", idusuario);
        java.util.List<Object[]> flujos = (java.util.List<Object[]>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
