/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Actividad;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class ActividadFacade {

    private Session session;

    public ActividadFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Actividad getActividadByID(int idactividad) {
        Actividad actividad = (Actividad) session.get(Actividad.class, idactividad);
        return actividad;
    }

    public List<Actividad> getActividadAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Actividad as actividad order by nombre");
        List<Actividad> actividads = (List<Actividad>) q.list();
        session.getTransaction().commit();
        return actividads;
    }    

    public List<Actividad> getActividadByProceso(int idproceso) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Actividad as actividad where proceso_idproceso = :idproceso order by orden");
        q.setInteger("idproceso", idproceso);
        List<Actividad> actividads = (List<Actividad>) q.list();
        session.getTransaction().commit();
        return actividads;
    }
    
    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
