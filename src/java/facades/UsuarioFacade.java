/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Usuario;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class UsuarioFacade {

    private Session session;

    public UsuarioFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Usuario getUsuarioByID(int idusuario) {
        Usuario usuario = (Usuario) session.get(Usuario.class, idusuario);
        return usuario;
    }
    
    public List<Usuario> getUsuarioAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Usuario as usuario order by nombre");
        List<Usuario> usuarios = (List<Usuario>) q.list();
        session.getTransaction().commit();
        return usuarios;
    }

    public Usuario getUsuarioByMailPass(String mail, String password) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Usuario as usuario where mail = :mail and pass = :password and fegreso is null");
        q.setString("mail", mail);
        q.setString("password", password);
        Usuario usuario = (Usuario) q.uniqueResult();
        session.getTransaction().commit();
        return usuario;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
