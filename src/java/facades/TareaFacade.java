/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Tarea;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class TareaFacade {

    private Session session;

    public TareaFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Tarea getTareaByID(int idtarea) {
        Tarea tarea = (Tarea) session.get(Tarea.class, idtarea);
        return tarea;
    }
    
    public Integer saveTarea(Tarea tarea) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(tarea);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateTarea(Tarea tarea) {
        try {
            session.getTransaction().begin();
            Tarea o = getTareaByID(tarea.getIdtarea());
            o.setEstado(tarea.getEstado());
            o.setFrecibido(tarea.getFrecibido());
            o.setFasignado(tarea.getFasignado());
            o.setFproceso(tarea.getFproceso());
            o.setFliberado(tarea.getFliberado());
            o.setFlujo(tarea.getFlujo());
            o.setUsuario(tarea.getUsuario());
            o.setActividad(tarea.getActividad());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Tarea> getTareaAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Tarea as tarea order by fasignacion");
        List<Tarea> tareas = (List<Tarea>) q.list();
        session.getTransaction().commit();
        return tareas;
    }
    
    public List<Tarea> getTareaByFlujo(int idflujo) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Tarea as tarea where flujo_idflujo = :idflujo");
        q.setInteger("idflujo", idflujo);
        List<Tarea> tareas = (List<Tarea>) q.list();
        session.getTransaction().commit();
        return tareas;
    }
    
    public Tarea getNextTareaByFlujo(int idflujo, int idtarea) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Tarea as tarea where flujo_idflujo = :idflujo and idtarea > :idtarea");
        q.setInteger("idflujo", idflujo);
        q.setInteger("idtarea", idtarea);
        q.setMaxResults(1);
        Tarea tarea = (Tarea) q.uniqueResult();
        session.getTransaction().commit();
        return tarea;
    }
    
    public Tarea getFirstTareaByFlujo(int idflujo) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Tarea as tarea where flujo_idflujo = :idflujo order by idtarea");
        q.setInteger("idflujo", idflujo);
        q.setMaxResults(1);
        Tarea tarea = (Tarea) q.uniqueResult();
        session.getTransaction().commit();
        return tarea;
    }
    
    public Tarea getPreviousTareaByFlujo(int idflujo, int idtarea) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Tarea as tarea where flujo_idflujo = :idflujo and idtarea < :idtarea order by idtarea DESC");
        q.setInteger("idflujo", idflujo);
        q.setInteger("idtarea", idtarea);
        q.setMaxResults(1);
        Tarea tarea = (Tarea) q.uniqueResult();
        session.getTransaction().commit();
        return tarea;
    }
    
    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
