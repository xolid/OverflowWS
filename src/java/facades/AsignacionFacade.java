/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Asignacion;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class AsignacionFacade {

    private Session session;

    public AsignacionFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Asignacion getAsignacionByID(int idasignacion) {
        Asignacion asignacion = (Asignacion) session.get(Asignacion.class, idasignacion);
        return asignacion;
    }

    public List<Asignacion> getAsignacionAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Asignacion as asignacion");
        List<Asignacion> asignacions = (List<Asignacion>) q.list();
        session.getTransaction().commit();
        return asignacions;
    }
    
    public List<Asignacion> getAsignacionByGrupo(int idgrupo) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Asignacion as asignacion where grupo_idgrupo = :idgrupo");
        q.setInteger("idgrupo", idgrupo);
        List<Asignacion> asignacions = (List<Asignacion>) q.list();
        session.getTransaction().commit();
        return asignacions;
    }
    
    public List<String> getAsignacionListMailByGrupo(int idgrupo) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT " +
                "usuario.mail " +
                "FROM " +
                "asignacion " +
                "inner join grupo on asignacion.grupo_idgrupo = grupo.idgrupo " +
                "inner join usuario on asignacion.usuario_idusuario = usuario.idusuario " +
                "WHERE " +
                "grupo.idgrupo = :idgrupo");
        q.setInteger("idgrupo", idgrupo);
        List<String> asignaciones = (List<String>) q.list();
        session.getTransaction().commit();
        return asignaciones;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
