/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Proceso;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class ProcesoFacade {

    private Session session;

    public ProcesoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Proceso getProcesoByID(int idproceso) {
        Proceso proceso = (Proceso) session.get(Proceso.class, idproceso);
        return proceso;
    }

    public List<Proceso> getProcesoAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Proceso as proceso order by nombre");
        List<Proceso> procesos = (List<Proceso>) q.list();
        session.getTransaction().commit();
        return procesos;
    }
    
    public Proceso getProcesoByNAME(String nombre) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Proceso as proceso where nombre = :nombre");
        q.setString("nombre", nombre);
        q.setMaxResults(1);
        Proceso proceso = (Proceso) q.uniqueResult();
        session.getTransaction().commit();
        return proceso;
    }
    
    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
