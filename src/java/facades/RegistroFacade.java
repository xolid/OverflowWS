/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Registro;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class RegistroFacade {

    private Session session;

    public RegistroFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Registro getRegistroByID(int idregistro) {
        Registro registro = (Registro) session.get(Registro.class, idregistro);
        return registro;
    }
    
    public Integer saveRegistro(Registro registro) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(registro);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public List<Registro> getRegistroAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Registro as registro order by fecha desc");
        List<Registro> registros = (List<Registro>) q.list();
        session.getTransaction().commit();
        return registros;
    }
    
    public List<Registro> getRegistroByFlujo(int idflujo) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Registro as registro where flujo_idflujo = :idflujo order by fecha desc");
        q.setInteger("idflujo", idflujo);
        List<Registro> registros = (List<Registro>) q.list();
        session.getTransaction().commit();
        return registros;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
