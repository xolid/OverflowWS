/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Grupo;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class GrupoFacade {

    private Session session;

    public GrupoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Grupo getGrupoByID(int idgrupo) {
        Grupo grupo = (Grupo) session.get(Grupo.class, idgrupo);
        return grupo;
    }

    public List<Grupo> getGrupoAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Grupo as grupo order by nombre");
        List<Grupo> grupos = (List<Grupo>) q.list();
        session.getTransaction().commit();
        return grupos;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
