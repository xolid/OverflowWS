/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Comentario;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class ComentarioFacade {

    private Session session;

    public ComentarioFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Comentario getComentarioByID(int idcomentario) {
        Comentario comentario = (Comentario) session.get(Comentario.class, idcomentario);
        return comentario;
    }
    
    public Integer saveComentario(Comentario comentario) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(comentario);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public List<Comentario> getComentarioAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Comentario as comentario order by fecha desc");
        List<Comentario> comentarios = (List<Comentario>) q.list();
        session.getTransaction().commit();
        return comentarios;
    }
    
    public List<Comentario> getComentarioByFlujo(int idflujo) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Comentario as comentario where flujo_idflujo = :idflujo order by fecha desc");
        q.setInteger("idflujo", idflujo);
        List<Comentario> comentarios = (List<Comentario>) q.list();
        session.getTransaction().commit();
        return comentarios;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
