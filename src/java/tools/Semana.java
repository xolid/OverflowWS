/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import properties.PropertiesManager;

/**
 *
 * @author solid
 */
public class Semana {
    
    private final int domingo;
    private final int lunes;
    private final int martes;
    private final int miercoles;
    private final int jueves;
    private final int viernes;
    private final int sabado;
    
    public Semana(){
        PropertiesManager pmanager = new PropertiesManager();
        domingo = Integer.parseInt(pmanager.getProperty("DOMINGO"));
        lunes = Integer.parseInt(pmanager.getProperty("LUNES"));
        martes = Integer.parseInt(pmanager.getProperty("MARTES"));
        miercoles = Integer.parseInt(pmanager.getProperty("MIERCOLES"));
        jueves = Integer.parseInt(pmanager.getProperty("JUEVES"));
        viernes = Integer.parseInt(pmanager.getProperty("VIERNES"));
        sabado = Integer.parseInt(pmanager.getProperty("SABADO"));
    }
    
    public boolean isWorkDay(int day){
        switch(day){
            case 1:
                return domingo==1;
            case 2:
                return lunes==1;
            case 3:
                return martes==1;
            case 4:
                return miercoles==1;
            case 5:
                return jueves==1;
            case 6:
                return viernes==1;
            case 7:
                return sabado==1;
        }
        return false;
    }
    
    public String getDisabledWeekDays(){
        java.util.List<String> strings = new java.util.ArrayList<>();
        for(int i=0;i<7;i++){
            if(!isWorkDay(i+1))
                strings.add(String.valueOf(i));
        }
        return String.join(",",strings);
    }
    
}
