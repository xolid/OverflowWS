/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import properties.PropertiesManager;

/**
 *
 * @author solid
 */
public class Hora {
    
    private PropertiesManager pmanager;
    private Semana semana;
    private java.util.Date entrada;
    private java.util.Date salida;
    private java.util.Date comida_salida;
    private java.util.Date comida_entrada;
    
    public Hora(){ }
    
    public Hora(java.util.Date fecha){
        pmanager = new PropertiesManager();
        semana = new Semana();
        entrada = setDate(pmanager.getProperty("ENTRADA"),fecha);
        salida = setDate(pmanager.getProperty("SALIDA"),fecha);
        comida_salida = setDate(pmanager.getProperty("COMIDA_SALIDA"),fecha);
        comida_entrada = setDate(pmanager.getProperty("COMIDA_ENTRADA"),fecha);
    }
    
    private java.util.Date setDate(String hora, java.util.Date fecha){
        String[] arrayHora = hora.split(":");
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.set(java.util.Calendar.HOUR_OF_DAY,Integer.parseInt(arrayHora[0]));
        calendar.set(java.util.Calendar.MINUTE,Integer.parseInt(arrayHora[1]));
        calendar.set(java.util.Calendar.SECOND,0);
        calendar.set(java.util.Calendar.MILLISECOND,0);
        return calendar.getTime();
    }
    
    public java.util.Date addHoursInValidTime(java.util.Date fecha, int hours){     
        java.util.Date local = fecha;
        java.util.Date local_entrada = setDate(pmanager.getProperty("ENTRADA"),local);
        java.util.Date local_salida = setDate(pmanager.getProperty("SALIDA"),local);
        java.util.Date local_comida_salida = setDate(pmanager.getProperty("COMIDA_SALIDA"),local);
        java.util.Date local_comida_entrada = setDate(pmanager.getProperty("COMIDA_ENTRADA"),local);
        for(int i=0;i<hours;i++){    
            local = addHour(local);
            if(local.after(local_salida)){
                int diferencia = diffMinutes(local_salida,local);
                local = local_entrada;
                local = addMinutes(local,diferencia);
                local = addDay(local);
                local_entrada = addDay(local_entrada);
                local_salida = addDay(local_salida);
                local_comida_salida = addDay(local_comida_salida);
                local_comida_entrada = addDay(local_comida_entrada);                             
                while(!semana.isWorkDay(getDayNumber(local))){
                    local = addDay(local);
                    local_entrada = addDay(local_entrada);
                    local_salida = addDay(local_salida);
                    local_comida_salida = addDay(local_comida_salida);
                    local_comida_entrada = addDay(local_comida_entrada);                             
                }
            }else{
                if(local.after(local_comida_salida) && local.before(local_comida_entrada)){
                    int diferencia = diffMinutes(local_comida_salida,local);
                    local = local_comida_entrada;
                    local = addMinutes(local,diferencia);
                }
            }
        }
        return local;    
    }
    
    public java.util.Date validHour(java.util.Date hora){
        if(hora.after(salida)){
            java.util.Calendar horaEntradaMañana = java.util.Calendar.getInstance();
            horaEntradaMañana.setTime(entrada);
            horaEntradaMañana.add(java.util.Calendar.DAY_OF_MONTH,1);
            while(!semana.isWorkDay(horaEntradaMañana.get(java.util.Calendar.DAY_OF_WEEK))){
                horaEntradaMañana.add(java.util.Calendar.DAY_OF_MONTH,1);
            }
            return horaEntradaMañana.getTime();
        }else{
            if(hora.before(entrada)){
                java.util.Calendar horaEntrada = java.util.Calendar.getInstance();
                horaEntrada.setTime(hora);
                if(!semana.isWorkDay(horaEntrada.get(java.util.Calendar.DAY_OF_WEEK))){
                    horaEntrada.setTime(entrada);
                    while(!semana.isWorkDay(horaEntrada.get(java.util.Calendar.DAY_OF_WEEK))){
                        horaEntrada.add(java.util.Calendar.DAY_OF_MONTH,1);
                    }
                }
                return setDate(pmanager.getProperty("ENTRADA"),horaEntrada.getTime());
            }else{
                if(hora.after(comida_salida) && hora.before(comida_entrada)){
                    java.util.Calendar horaComida = java.util.Calendar.getInstance();
                    horaComida.setTime(hora);
                    if(!semana.isWorkDay(horaComida.get(java.util.Calendar.DAY_OF_WEEK))){
                        horaComida.setTime(entrada);
                        while(!semana.isWorkDay(horaComida.get(java.util.Calendar.DAY_OF_WEEK))){
                            horaComida.add(java.util.Calendar.DAY_OF_MONTH,1);
                        }
                    }
                    return setDate(pmanager.getProperty("COMIDA_ENTRADA"),horaComida.getTime());
                }else{
                    java.util.Calendar ahora = java.util.Calendar.getInstance();
                    ahora.setTime(hora);
                    if(!semana.isWorkDay(ahora.get(java.util.Calendar.DAY_OF_WEEK))){
                        ahora.setTime(entrada);
                        while(!semana.isWorkDay(ahora.get(java.util.Calendar.DAY_OF_WEEK))){
                            ahora.add(java.util.Calendar.DAY_OF_MONTH,1);
                        }
                    }
                    return ahora.getTime();
                }
            }
        }
    }
    
    private java.util.Date addMinutes(java.util.Date fecha,int minutes){
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(java.util.Calendar.MINUTE,minutes);
        return calendar.getTime();
    }
    
    private java.util.Date addHour(java.util.Date fecha){
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(java.util.Calendar.HOUR_OF_DAY,1);
        return calendar.getTime();
    }
    
    private java.util.Date addDay(java.util.Date fecha){
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(java.util.Calendar.DAY_OF_MONTH,1);
        return calendar.getTime();
    }
    
    private int getDayNumber(java.util.Date fecha){
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(fecha);
        return calendar.get(java.util.Calendar.DAY_OF_WEEK);
    }
    
    public int diffMinutes(java.util.Date fecha1 ,java.util.Date fecha2){
        long startTime = fecha1.getTime();
        long endTime = fecha2.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60);
        return (int)diffDays;
    }
    
    private boolean isActiveHour(java.util.Date hora){
        if(hora.before(entrada)||hora.after(salida)){
            return false;
        }
        if(hora.after(comida_salida)&&hora.before(comida_entrada)){
            return false;
        }
        return true;
    }
    
    public String getEnableHours(){
        java.util.List<String> strings = new java.util.ArrayList<>();
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(java.util.Calendar.MINUTE,0);
        calendar.set(java.util.Calendar.SECOND,0);
        calendar.set(java.util.Calendar.MILLISECOND,0);
        for(int i=0;i<24;i++){
            calendar.set(java.util.Calendar.HOUR_OF_DAY,i);
            if(isActiveHour(calendar.getTime()))
                strings.add("'"+new java.text.SimpleDateFormat("HH:mm").format(calendar.getTime())+"'");
        }
        return String.join(",",strings);
    }
    
}
